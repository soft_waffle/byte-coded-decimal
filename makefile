CFLAGS = -g -std=c89
DEPS = bcd.h

%.o: %.c $(DEPS)
	$(CC) -c -o $@ $< $(CFLAGS)

tests: tests.o bcd.o
	$(CC) $^ -o $@

clean:
	rm -r *.o tests
