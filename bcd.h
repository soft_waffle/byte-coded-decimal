#ifndef BCD_H
#define BCD_H

#include <stdint.h>
#include <stddef.h>

typedef union Bcd {
	uint32_t bits;
	uint8_t digits[4];
} BCD;

/*
 * NOTE TO SELF: The last index is the first digit
 *
 * ----
 * 1023
 *    ^ for the example, this is index 3
 * ----
 *
 * The units are last, decimals are pre-last, etc.
 */

static const BCD BCD_HUNDRED = { .digits = { 0, 0, 1, 0 } };
static const BCD BCD_ONE     = { .digits = { 0, 0, 0, 1 } };
static const BCD BCD_TEN     = { .digits = { 0, 0, 0, 0x10 } };
static const BCD BCD_ZERO    = { .digits = { 0, 0, 0, 0 } };

uint8_t BCD_get_digit (BCD this, size_t index);
void    BCD_set_digit (BCD* this, size_t i, uint8_t digit);

int BCD_lt (BCD this, BCD that);
int BCD_gt (BCD this, BCD that);
int BCD_eq (BCD this, BCD that);

BCD BCD_add       (BCD this, BCD that);
BCD BCD_substract (BCD this, BCD that);

BCD BCD_multiply  (BCD this, BCD that);
BCD BCD_dividie   (BCD this, BCD that);

BCD BCD_from_string (const char string[], size_t length);
void BCD_tostring (BCD this, char string[], size_t length);
uint32_t BCD_tonumber (BCD this);

#endif /* BCD_H */
/* vim: set ft=c */
