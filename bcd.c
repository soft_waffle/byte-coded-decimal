#include <stdio.h>
#include <stddef.h>

#include "bcd.h"

uint8_t BCD_get_digit (BCD this, size_t i) {
	size_t j;

	j = i % 2;
	i = 7 - i;
	i /= 2;

	return (this.digits[i] >> (j * 4)) & 0xF;
}

void BCD_set_digit (BCD* this, size_t i, uint8_t digit) {
	size_t j;
	uint8_t save_mask;
	uint8_t set_mask;

	j = i % 2;
	i = 7 - i;
	i /= 2;

	if (j == 0) {
		this->digits[i] = (this->digits[i] & 0xF0) | (digit & 0xF);
	} else {
		this->digits[i] = (this->digits[i] & 0xF) | (digit << 4) & 0xF0;
	}
}

int BCD_lt (BCD this, BCD that) {
	size_t i;
	uint8_t this_digit, that_digit;

	for (i = 8; i > 0; i--) {
		this_digit = BCD_get_digit (this, i - 1);
		that_digit = BCD_get_digit (that, i - 1);
		if (this_digit != that_digit) {
			return this_digit < that_digit;
		}
	}
	return 0;
}

int BCD_gt (BCD this, BCD that) {
	size_t i;
	uint8_t this_digit, that_digit;

	for (i = 8; i > 0; i--) {
		this_digit = BCD_get_digit (this, i - 1);
		that_digit = BCD_get_digit (that, i - 1);

		if (this_digit != that_digit) {
			return this_digit > that_digit;
		}
	}
	return 0;
}

int BCD_eq (BCD this, BCD that) {
	return this.bits == that.bits;
}

BCD BCD_add (BCD this, BCD that) {
	size_t i;
	uint8_t sum;
	uint8_t accumulated;
	BCD result;

	result = BCD_ZERO;
	accumulated = 0;
	for (i = 0; i < 8; i++) {
		sum  = BCD_get_digit (this, i);
		sum += BCD_get_digit (that, i);
		sum += accumulated;

		accumulated = sum > 9 ? sum - 9 : 0;
		if (sum > 9) {
			sum -= accumulated;
		}
		BCD_set_digit (&result, i, sum);
	}
	return result;
}

uint32_t BCD_tonumber (BCD this) {
	uint32_t result;
	size_t power_of_ten;
	size_t i;

	power_of_ten = 1;
	result = 0;
	for (i = 0; i < 8; i++) {
		result += BCD_get_digit (this, i) * power_of_ten;
		power_of_ten *= 10;
	}
	return result;
}

#define FOR_RANGE(var, begin, end, step) for (var = begin; var < end; var += step)
BCD BCD_from_string (const char string[], size_t length) {
	size_t i, j;
	BCD result = { .bits = 0 };
	length = length < 8 ? length : 8;
	FOR_RANGE(i, 0, length, +1) {
		j = 8 - (length - i);
		result.digits[j/2] = (string[i] - '0' << (!(j%2)*4)) | result.digits[j/2];
	}
	return result;
}
