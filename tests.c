#include <stdio.h>
#include "bcd.h"

#define ASSERT(expr) do {\
	if (!(expr)) {\
		printf("assertion failed at line %d, expr: %s\n", __LINE__, #expr);\
		return 1;\
	}\
} while (0)

#define ASSERT_NOT(expr) do {\
	if (expr) {\
		printf("assertion failed at line %d, expr: %s\n", __LINE__, #expr);\
		return 1;\
	}\
} while (0)

int main () {
	/*
	 * TODO: TESTS
	 */

	/* Equals */
	ASSERT (BCD_eq (BCD_TEN, BCD_TEN));
	ASSERT (BCD_eq (BCD_HUNDRED, BCD_HUNDRED));
	ASSERT_NOT (BCD_eq (BCD_HUNDRED, BCD_TEN));

	/* To number */
	ASSERT (BCD_tonumber (BCD_HUNDRED) == 100);
	ASSERT (BCD_tonumber (BCD_ONE) == 1);
	ASSERT (BCD_tonumber (BCD_add (BCD_HUNDRED, BCD_ONE)) == 101);

	/* From string */
	ASSERT (BCD_eq (BCD_ONE, BCD_from_string ("1", 1)));
	ASSERT (BCD_eq (BCD_TEN, BCD_from_string ("10", 2)));
	ASSERT (BCD_eq (BCD_HUNDRED, BCD_from_string ("100", 3)));
	ASSERT (BCD_eq (BCD_add (BCD_HUNDRED, BCD_ONE), BCD_from_string ("101", 3)));

	/* Set digit */
	BCD num = BCD_ZERO;
	BCD_set_digit (&num, 3, 1);
	ASSERT (BCD_tonumber (num) == 1000);

	/* Less than */
	ASSERT (BCD_lt (BCD_ONE, BCD_HUNDRED));
	ASSERT (BCD_lt (BCD_ONE, BCD_HUNDRED));
	ASSERT (BCD_lt (BCD_ONE, BCD_TEN));
	ASSERT_NOT (BCD_lt (BCD_TEN, BCD_ONE));

	ASSERT (BCD_tonumber (BCD_add (BCD_ONE, BCD_TEN)) == 11);
	return 0;
}
